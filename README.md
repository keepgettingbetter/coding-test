coding-test
-----------

Create a PHP version of the [Split App](https://play.google.com/store/apps/details?id=splid.teamturtle.com.splid) an application in which you register expenses per person. When you
press the "settle" button the app calculates who needs to pay/owe which amount of money.

So what's basically needed is:

    - Some place to register expenses per person (name of person is enough here)
    - A button that triggers the calculation and displays the outcome (who needs to pay who)

The look and feel of the application don't matter at all. Of course you are free to use any frontend
framework (like Twitter Bootstrap) to make things a little bit more prettier. That's up to you.

In this repository is included:

    - Symfony 4.4
    - Twig templating engine
    - Doctrine ORM

If you don't feel comfortable to use Symfony, just overwrite the `public/index.php` file with your own code.

If you want to use Symfony, most easy way to get started is to following the instructions per https://symfony.com/doc/current/setup.html#running-symfony-applications
This requires the `symfony` binary to be installed locally.

When you run the Symfony server with `symfony server:start` the default Symfony page can be reached at:
https://localhost:8000/

Keep in mind there is no good or wrong for this test. No worries if you don't finish in time.

If you have any questions: let us know... we're happy to help! Nobody knows everything. 

In case you have time left, try these other nice to haves:

    - introduce Docker to run php, webserver, database
    - if you didn't use Symfony yet, use Symfony
    - prove the application works correctly by adding a test
    - introduce React JS for the frontend
